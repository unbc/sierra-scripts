<#
Convert-CSVForSierraFoundInventory script
Written by David Pettitt

Written for Vaunda Dumont (Circulation Supervisor)

Converts files created by Mobile Worklists app and CS3070 "phaser" scanner into a format that can be imported in Sierra for Inventory. Also merges contents of multiple csv files into one output.

Reference:
https://csdirect.iii.com/documentation/rdiformats.php

#>

$CSVFiles = Get-ChildItem *.csv
$barcode_prefix = "n:"
$newfile = "rdi_mergedoutput.txt"
$csv_merged = @()

Write-Host "Converting and merging .csv files..."

foreach ($file in $CSVFiles){
	
	$csv = Import-CSV $file
	
	# Check if it's a Mobile Worklists file, with proper headers
	if ($csv[0].barcode -ne $null){
		# Re-import, just the Barcode column
		$csv = @()
		$csv += Import-CSV $file | select -ExpandProperty 'Barcode'
	}
	# Tries to import CS3070 "phaser" scanner file
	else{
		$csv = @()
		$csv += Import-CSV $file -Header "Date","Time","Blah","Barcode" | Select -ExpandProperty "Barcode"
	}
	
	# Makes sure there's actually something in $csv variable, quit if not
	if ($csv.count -eq 0){
		break
	}
	
	for ($i=0; $i -lt $csv.count; $i++){
		$csv[$i] = $barcode_prefix + $csv[$i]
	}
	
	$csv_merged += $csv
}

$csv_merged | Out-File $newfile -Encoding ASCII