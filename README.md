# Sierra Scripts

## Overview

**Purpose:** This is a collection of PowerShell scripts developed to assist with various circulation tasks in the Sierra ILS (from Innovative Interfaces, Inc.).

* **Convert-CSVForSierraFoundInventory.ps1:** Old script, probably not useful to anyone. Should probably remove it from this repo.
* **Convert-CSVForSierraInventory.ps1:** Convert .csv files containing barcodes into a format that can be imported into and used by the Sierra Admin Corner function "Compare Inventory to Shelf List"
* **Convert-CSVForSierraReviewFile.ps1:** Convert .csv files containing barcodes into a format that can be imported into a Sierra Desktop Application Review File
* **Convert-CSVForSierraUseCountStats.ps1:** Convert .csv file(s) containing barcodes into a format that can be loaded by the Sierra Desktop Application function "Count Use - Portable Reader"
* **Create-SierraInventoryReports.ps1:** Ingest .csv file(s) containing barcodes and generate: 1) report(s) that duplicate the Sierra Admin Corner "Compare Inventory to Shelf List" report, and 2) a file that can be imported into a Sierra Desktop Application Review File

---

## General Usage/Requirements/Notes

These are basic requiremants and usage instructions for all the scripts. Additional requirements, instructions and notes for specific scripts are detailed further below.

### Requirements
* Windows computer with PowerShell 5.1. These scripts may work on other versions of PowerShell, but have not been tested on anything besides PS 5.1
* The ability to launch PowerShell scripts. This may require talking to your IT folk.

### Usage
1. Place the script(s) you wish to use into a folder
2. Into the same folder, place the .csv file(s) you wish to process (all the scripts are configured to process all .csv files located in the same folder)
3. Right-click the desired script and select "Run with PowerShell"
4. Alternatively, shift-right-click on a blank area in Windows Explorer and select "Open PowerShell window here". From the PowerShell prompt, type the name of the script you wish to run.
5. Assuming the script completes successfully, it will have generated one or more output files in the same folder (depending on which script you ran)

### Notes
* All the scripts will successfully ingest .csv files created by the Motorola/Zebra CS3070 portable barcode scanner, the Cognex Scanner iOS app, and the Mobile Worklists iOS app. They will likely require modification to handle other .csv files.

---

## Convert-CSVForSierraReviewFile & Create-SierraInventoryReports.ps1

### Requirements
In addition to the general requirements above, these scripts also require:

* a Sierra account with assigned application "Sierra SQL Access"
* SQL access to the Sierra DB server from the computer used to run the scripts
* a one-time installation of the PostgreSQL x64 ODBC driver (https://www.postgresql.org/ftp/odbc/versions/msi/) (No configuration needed after installation)

### Notes
* You will need to make a one-time modification of the scripts (specifically the Get-ODBCData function) to have the appropriate Sierra DB servername, port and dbname for your Sierra instance
* For **Create-SierraInventoryReports.ps1**, you may also need to modify the SQL queries to fit your particular Sierra configuration.

---

## Rights

* Copyright © 2018 University of Northern British Columbia
* Released under an MIT license: https://opensource.org/licenses/MIT
* Developed by David Pettitt, Systems Administrator I, Geoffrey R. Weller Library at UNBC

### Apologia

I (David) am not a software developer by training or inclination. I write small scripts to make life easier for myself and my co-workers. If you notice a more efficient/standards-adhering way to achieve the same results, a) great! and b) please let me know.

---

## Feedback/Questions

Contact: <david.pettitt@unbc.ca>

Please let me know if you find these tools helpful and/or if you find any issues.