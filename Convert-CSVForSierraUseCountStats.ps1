<#
Convert-UseCountStatsFilesForSierra script
Written by David Pettitt

Written for Vaunda Dumont and Circulation Staff

Converts files created by CS3070 "phaser" scanner, Cognex Scanner barcode scanning iOS app, or Mobile Worklists iOS app into a format that can be imported in Sierra in the 'Count Use - Portable Reader' screen

Reference:
https://csdirect.iii.com/documentation/rdiformats.php

#>

$CSVFiles = Get-ChildItem *.csv
$barcode_prefix = "c:"
$newfile = "rdu_mergedoutput.txt"
$csv_merged = @()
$bad_barcodes = @()

Write-Host "Converting and merging .csv files..."

foreach ($file in $CSVFiles){
	
	$csv = Import-CSV $file
	
	# Check if it's a Mobile Worklists file, with proper headers
	if ($csv.count -gt 0 -and $csv[0].barcode -ne $null){
		# Re-import, just the Barcode column
		$csv = @()
		$csv += Import-CSV $file | select -ExpandProperty 'Barcode'
	}
	# Check if it's from Cognex Scanner iOS app
	elseif ($csv.count -gt 0 -and $csv[0]."BarcodeScanners History" -ne $null){
		$csv = @()
		$csv += Get-Content $file | Select-Object -Skip 2 | ConvertFrom-CSV | Select -ExpandProperty "Code"
	}
	# Assumes it's a CS3070 "phaser" scanner file
	else{
		$csv = @()
		$csv += Import-CSV $file -Header "Date","Time","Blah","Barcode" | Select -ExpandProperty "Barcode"	
	}
	
	# Makes sure there's actually something in $csv variable, quit if not
	if ($csv.count -eq 0){
		Write-Host ""
		Write-Host "No barcodes found in "$file.name
		Write-Host ""
		break
	}
	
	# Convert all valid barcodes, remove invalid ones
	for ($i=0; $i -lt $csv.count; $i++){
		if ($csv[$i] -match '\d{14}'){
			$csv[$i] = $barcode_prefix + $csv[$i]
		}
		else{
			$bad_barcodes += $csv[$i]
            Write-Host "Invalid barcode" $csv[$i] "in" $file.name
		}
	}

    # Remove invalid barcodes
    foreach ($bad_code in $bad_barcodes){
        $csv = $csv | Where-Object { $_ -ne $bad_code }
    }

	
	$csv_merged += $csv	
}

$csv_merged | Out-File $newfile -Encoding ASCII

Get-Date
Write-Host "Output file:" $newfile
Write-Host "# of invalid barcodes:" $bad_barcodes.count
Write-Host "# of valid barcodes:" $csv_merged.count
Write-Host ""
	
Write-Host "Press a key to continue..." -ForegroundColor Yellow
$pause = $host.ui.RawUI.ReadKey("NoEcho,IncludeKeyDown")