<#
Convert-CSVForSierraInventory script
Written by David Pettitt

Written for Vaunda Dumont (Circulation Supervisor)

Converts files created by CS3070 "phaser" scanner, Cognex Scanner barcode scanning iOS app, or Mobile Worklists iOS app into a format that can be imported in Sierra for the Admin Corner "Compare Inventory to Shelf List" function.

Reference:
https://csdirect.iii.com/documentation/rdiformats.php

#>

$CSVFiles = Get-ChildItem *.csv
$barcode_prefix = "n:"
$filename_prefix = "rdi"

Write-Host "Converting .csv file(s)..."

foreach ($file in $CSVFiles){
	Write-Host ""
	Write-Host "Input file"$file.name
	$newfile = $filename_prefix + $file.BaseName + ".txt"
	
	$csv = Import-CSV $file
	
	# Makes sure there's actually something in $csv variable, quit if not
	if ($csv.count -eq 0){
		Write-Host "Source file"$file.name"is empty"
		Write-Host "Press a key to continue..." -ForegroundColor Yellow
	    $pause = $host.ui.RawUI.ReadKey("NoEcho,IncludeKeyDown")
		break
	}
	
	# Check if it's a Mobile Worklists file, with proper headers
	if ($csv.count -gt 0 -and $csv[0].barcode -ne $null){
		# Re-import, just the Barcode column
		$csv = @()
		$csv += Import-CSV $file | select -ExpandProperty 'Barcode'
	}
	# Check if it's from Cognex Scanner iOS app
	elseif ($csv.count -gt 0 -and $csv[0]."BarcodeScanners History" -ne $null){
		$csv = @()
		$csv += Get-Content $file | Select-Object -Skip 2 | ConvertFrom-CSV | Select -ExpandProperty "Code"
	}
	# Assumes it's a CS3070 "phaser" scanner file
	else{
		$csv = @()
		$csv += Import-CSV $file -Header "Date","Time","Blah","Barcode" | Select -ExpandProperty "Barcode"
	}
	
	# Makes sure there's actually something in $csv variable, quit if not
	if ($csv.count -eq 0){
		Write-Host "No barcodes found in source file"
		Write-Host "Press a key to continue..." -ForegroundColor Yellow
	    $pause = $host.ui.RawUI.ReadKey("NoEcho,IncludeKeyDown")
		break
	}
	
	# counter to track invalid barcodes found
	$invalid_code_count = 0
	# new array to hold valid barcode values
	$new_csv = @()
	
	for ($i=0; $i -lt $csv.count; $i++){
		#Check if barcode is valid (14 digits)
		if ($csv[$i] -match '^\d{14}$'){
			$new_csv += $barcode_prefix + $csv[$i]
		}
		else{
			$invalid_code_count++
			Write-Host "Invalid barcode" $csv[$i] "on line" ($i+1) "of" $file.name
		}
	}
	
	
	$new_csv | Out-File $newfile -Encoding ASCII
	
	Write-Host ""
	Get-Date
	Write-Host "Output file: "$newfile
	Write-Host "# of valid barcodes: "$new_csv.count
	Write-Host "# of invalid barcodes: "$invalid_code_count
	Write-Host ""
	
	Write-Host "Press a key to continue..." -ForegroundColor Yellow
	$pause = $host.ui.RawUI.ReadKey("NoEcho,IncludeKeyDown")
}