<#
Create-SierraInventoryReports 
Written by David Pettitt

Written for Vaunda Dumont (Circulation Supervisor)

Ingests one or more .csv files from CS3070 'phaser' scanner, Cognex Scanner barcode scanning iOS app, or Mobile Worklists iOS app. Compares that list to the range of 'on-shelf' items in Sierra. Generates a report file (one per .csv input) listing items that need action from Circulation. Generates a file of items that can be imported into a Sierra Review File to Rapid Update the Inventory Date.

For this to work, you need to download and install an x64 ODBC driver from https://www.postgresql.org/ftp/odbc/versions/msi/
No configuration in Windows is necessary after installing the driver

Reference:
http://blog.briankmarsh.com/postgresql-powershell-part-1/

#>

# This is for PostgreSQL queries that item records
function Get-ODBCData{  
    param(
          [string]$query,    # Query
          [string]$dbServer = 'wizarddb.unbc.ca',   # DB Server (either IP or hostname)
          [int]$dbPort = 1032, # DB Connection Port
          [string]$dbName   = 'iii', # Name of the database
          [string]$dbUser   = 'reports',    # User we'll use to connect to the database/server
          [string]$dbPass    # Password for the $dbUser
         )

    $conn = New-Object System.Data.Odbc.OdbcConnection
    $conn.ConnectionString = "Driver={PostgreSQL Unicode(x64)};Server=$dbServer;Port=$dbPort;Database=$dbName;Uid=$dbUser;Pwd=$dbPass;sslmode=require;"
    $conn.open()
    $cmd = New-object System.Data.Odbc.OdbcCommand($query,$conn)
    $ds = New-Object system.Data.DataSet
    $Timeout = 180
    do{
        $cmd.CommandTimeout = $Timeout
        try{
            Write-Host ('Querying database with ' + $Timeout + '-second timeout...')
            Write-Host ''
            (New-Object system.Data.odbc.odbcDataAdapter($cmd)).fill($ds) | out-null
            $Timeout = 300 # This will break us out of do loop, on success
        }
        catch{
            $Timeout += 60
        }
    }while ($Timeout -le 240)
    
    $conn.close()
    $ds.Tables[0]
}

# Truncates strings to specified length, if necessary
function Truncate-Value{
    param( 
            [string]$Value,
            [int]$Length
    )
    if ( $Value.length -gt $Length ){
        $Value.Substring(0,$Length)
    }
    else{
        $Value
    }
}

# Make Call Numbers look nicer (Uppercase, eliminate extraneous spaces)
function Prettify-CallNumbers{
    param( [PSObject]$items )

    foreach ($item in $items){
        # Uppercase everything
        # First Replace: eliminate first space
        # Second Replace: Get rid of multiple spaces
        $item.CallNumberFull = $item.CallNumberFull.ToUpper() -Replace "(?<=^\w+)(\s+)(?=\d+)",'' -Replace "\s{2,}",' '
        
        #[regex]$pattern="(?<=[\w\d\.]+)( +)"
        #$item.CallNumberFull = $pattern.replace($item.CallNumberFull, '.', 1)
    }

    $items
}

Set-StrictMode -Version Latest
$ErrorActionPreference = 'Stop'

try{
    # Initialize a new Review File Item List file
    $ReviewFile = 'ReviewFileItemList.txt'
    Out-File $ReviewFile -Encoding ASCII

    # Get the file(s) to process
    if (Test-Path InventoryTest){
        $CSVFiles = @(Get-ChildItem InventoryTest\*.csv)
    }
    else{
        $CSVFiles = @(Get-ChildItem *.csv)
    }

    if ($CSVFiles.count -eq 0){
        Write-Warning 'No CSV files found'
        Write-Host ''

        # Pause for user interaction before exiting script
        Write-Host 'Press a key to continue...' -ForegroundColor Yellow
        $pause = $host.ui.RawUI.ReadKey('NoEcho,IncludeKeyDown')	
        break
    }

    # Get the Sierra credentials that will be used to connect to database
    # Load from encrypted file if possible. If file doesn't exist, prompt and then save to file for next time
    $CredFile = 'Sierra_DB_Credentials_' + $env:Username + '_' + $env:ComputerName + '.cred'
    if (Test-Path $CredFile){
        $SierraCred = Import-CliXML -Path $CredFile
    }
    else{
        $SierraCred = Get-Credential -message 'Enter Sierra login credentials:'
        if ($null -ne $SierraCred){
            $SierraCred | Export-CliXML -Path $CredFile
        }
        else{
            Throw 'Sierra login credentials are required'
        }
    }

    # Process the files
    foreach ($file in $CSVFiles){
        # Do an initial import of the file
        Write-Host 'Checking '$file' ...'
        $csv = Import-CSV $file
        $LineCount = (Get-Content $File | Measure-Object -Line).Lines
        
        if ($LineCount -eq 0 -and $null -eq $csv){
            Write-Warning ('The file ' + $file + ' is empty, skipping...')
            continue
        }
        elseif ($LineCount -gt 1 -and ($csv | Get-Member -MemberType NoteProperty).Name -contains 'barcode'){
            # It's a Mobile Worklists file, with proper headers
            $barcodes_unique = $csv.Barcode | Get-Unique
        }
        elseif ($LineCount -gt 1 -and ($csv | Get-Member -MemberType NoteProperty).Name -contains 'BarcodeScanner History'){
            # It's from Cognex Scanner iOS app
            $scan = @()
            # Skip the top two useless lines when importing Cognex csv
            $scan += Get-Content $file | Select-Object -Skip 2 | ConvertFrom-CSV
            # Remove extraneous final entry
            $scan = $scan | Where-Object {$_.Symbology -ne 'Cognex Corporation' -and $_.Symbology -ne ''}

            # Make sure scanned barcodes are in the correct order
            if ( (Get-Date($scan[0].Date)) -gt (Get-Date($scan[$scan.count - 1].Date)) ){
                Write-Warning 'Scanned barcodes are in reverse order, unable to process'
                Write-Host ''
            
                # Pause for user interaction before exiting script
                Write-Host 'Press a key to continue...' -ForegroundColor Yellow
                $pause = $host.ui.RawUI.ReadKey('NoEcho,IncludeKeyDown')	
                break
            }

            $barcodes_unique = $scan.Code | Get-Unique
        }
        else{
            # Must be a CS3070 'phaser' scanner file, or similar
            $scan = @()
            $scan += Import-CSV $file -Header 'Date','Time','Unknown','Barcode'

            $barcodes_unique = $scan.Barcode | Get-Unique
        }

        # Merge barcodes into single string, to use in SQL query
        $barcodes = "('" + ($barcodes_unique -join "'),('") + "')"
        
        # Clean up barcodes list to remove empty entries and doubled/trailing commas
        $barcodes = $barcodes -Replace "\(''\)",'' -Replace ',,',',' -Replace ',$',''

        # SQL query to get records for all scanned barcodes
        $query = @"
SELECT 'i' || item_view.record_num AS item_record,
scanned_barcode AS barcode,
COALESCE(call_number_norm, pe.index_entry) AS CallNumber,
concat(COALESCE(call_number_norm, pe.index_entry), ' ', vv2.field_content, ' c.', copy_num) AS CallNumberFull,
bib_view.title,
location_code,
ipm.name AS IType,
item_view.item_status_code,
checkout.loanrule_code_num as LoanRuleCode,
ispm.name AS Status,
vv3.field_content AS marc_tag_830,
is_suppressed
    FROM (
        values $barcodes
    ) scanned(scanned_barcode)
    LEFT JOIN sierra_view.item_record_property as item_record_property ON item_record_property.barcode = scanned.scanned_barcode
    LEFT JOIN sierra_view.item_view AS item_view ON item_view.id = item_record_property.item_record_id
    LEFT JOIN sierra_view.checkout AS checkout ON checkout.item_record_id = item_record_property.item_record_id
    LEFT JOIN sierra_view.itype_property_myuser AS ipm ON item_view.itype_code_num = ipm.code 
    LEFT JOIN sierra_view.item_status_property_myuser AS ispm ON item_view.item_status_code = ispm.code
    LEFT JOIN sierra_view.bib_record_item_record_link AS brirl ON item_view.id = brirl.item_record_id
    LEFT JOIN sierra_view.bib_view AS bib_view ON brirl.bib_record_id = bib_view.id
    LEFT JOIN sierra_view.phrase_entry AS pe ON pe.record_id = brirl.bib_record_id AND pe.index_tag = 'c' AND pe.occurrence = 0
    LEFT JOIN sierra_view.varfield_view AS vv2 ON item_view.record_num = vv2.record_num AND vv2.record_type_code = 'i' AND vv2.varfield_type_code = 'v' -- Item Volume
    LEFT JOIN sierra_view.varfield_view AS vv3 ON bib_view.record_num = vv3.record_num AND vv3.marc_tag = '830' AND vv3.field_content ilike '%university of northern%'
;
"@

        # Query the database for the provided list of scanned barcodes
        Write-Host 'Querying database for scanned item details...'
        $scanned_items = Get-ODBCData -query $query -dbUser $SierraCred.Username -dbPass $SierraCred.GetNetworkCredential().Password

        # Use function to make the call numbers look nicer
        $scanned_items = Prettify-CallNumbers $scanned_items

        # Add sort numbers to scanned items
        $scan_order = 1
        foreach ($scanned_item in $scanned_items){
            $scanned_item | Add-Member -NotePropertyName 'ScanOrder' -NotePropertyValue $scan_order
            $scan_order++
        }
        
        # Get the first and last callnumbers, locations, barcodes from the list of scanned barcodes
        $callnum_begin = $scanned_items[0].CallNumber
        $callnumfull_begin = $scanned_items[0].CallNumberFull
        $loc_begin = $scanned_items[0].location_code
        $barcode_begin = $scanned_items[0].barcode
        $callnum_end = $scanned_items[$scanned_items.count - 1].CallNumber
        $callnumfull_end = $scanned_items[$scanned_items.count - 1].CallNumberFull
        $loc_end = $scanned_items[$scanned_items.count - 1].location_code
        $barcode_end = $scanned_items[$scanned_items.count - 1].barcode

        # Make sure neither callnumber is blank, both are from same location, and first comes before last
        if ( $callnumfull_begin -eq ' C.' -OR $callnumfull_end -eq ' C.' -OR ($loc_begin -ne $loc_end) -OR ($callnum_begin -gt $callnum_end) ){
            Write-Warning ('There is a problem with the item records in ' + $file)
            Write-Host 'First callnumber:' $callnum_begin
            Write-Host 'First location:' $loc_begin
            Write-Host 'First barcode:' $barcode_begin
            Write-Host ''
            Write-Host 'Last callnumber:' $callnum_end
            Write-Host 'Last location:' $loc_end
            Write-Host 'Last barcode:' $barcode_end
            Write-Host ''

            # Pause for user interaction before exiting script
            Write-Host 'Press a key to continue...' -ForegroundColor Yellow
            $pause = $host.ui.RawUI.ReadKey('NoEcho,IncludeKeyDown')	
            break
        }

        # SQL query to get records for all off-shelf status items within the scanned range
        $query = @"
SELECT 'i' || item_view.record_num AS item_record,
item_view.barcode, 
COALESCE(call_number_norm, pe.index_entry) AS CallNumber,
location_code, 
item_view.item_status_code,
is_suppressed
    FROM sierra_view.item_record_property as item_record_property
    LEFT JOIN sierra_view.item_view AS item_view ON item_view.id = item_record_property.item_record_id
    LEFT JOIN sierra_view.bib_record_item_record_link AS brirl ON item_view.id = brirl.item_record_id
    LEFT JOIN sierra_view.phrase_entry AS pe ON pe.record_id = brirl.bib_record_id AND pe.index_tag = 'c' AND pe.occurrence = 0
    WHERE item_status_code NOT IN ('-','o') AND location_code = '$loc_begin' AND is_suppressed = false
    AND COALESCE(call_number_norm, pe.index_entry) BETWEEN '$callnum_begin' AND '$callnum_end'
;
"@

        # Query the database for the range of records
        Write-Host 'Querying database for all off-shelf item records in scanned item range...'
        $offshelf_items = @(Get-ODBCData -query $query -dbUser $SierraCred.Username -dbPass $SierraCred.GetNetworkCredential().Password)

        # SQL query to get records for all in-library items within the scanned range
        $query = @"
SELECT 'i' || item_view.record_num AS item_record,
item_view.barcode,
COALESCE(call_number_norm, pe.index_entry) AS CallNumber,
concat(COALESCE(call_number_norm, pe.index_entry), ' ', vv2.field_content, ' c.', copy_num) AS CallNumberFull,
bib_view.title,
location_code,
itype_property_name.name AS IType,
item_view.item_status_code,
checkout.loanrule_code_num as LoanRuleCode,
item_status_property_name.name AS Status,
vv3.field_content AS marc_tag_830,
is_suppressed
    FROM sierra_view.item_record_property as item_record_property
    LEFT JOIN sierra_view.item_view AS item_view ON item_view.id = item_record_property.item_record_id
    LEFT JOIN sierra_view.checkout AS checkout ON checkout.item_record_id = item_record_property.item_record_id
    LEFT JOIN sierra_view.itype_property AS itype_property ON item_view.itype_code_num = itype_property.code_num
    LEFT JOIN sierra_view.itype_property_name AS itype_property_name ON itype_property.id = itype_property_name.itype_property_id
    LEFT JOIN sierra_view.item_status_property AS item_status_property ON item_status_code = item_status_property.code
    LEFT JOIN sierra_view.item_status_property_name AS item_status_property_name ON item_status_property.id = item_status_property_name.item_status_property_id
    LEFT JOIN sierra_view.bib_record_item_record_link AS brirl ON item_view.id = brirl.item_record_id
    LEFT JOIN sierra_view.bib_view AS bib_view ON brirl.bib_record_id = bib_view.id
    LEFT JOIN sierra_view.phrase_entry AS pe ON pe.record_id = brirl.bib_record_id AND pe.index_tag = 'c' AND pe.occurrence = 0
    LEFT JOIN sierra_view.varfield_view AS vv2 ON item_view.record_num = vv2.record_num AND vv2.record_type_code = 'i' AND vv2.varfield_type_code = 'v' -- Item Volume
    LEFT JOIN sierra_view.varfield_view AS vv3 ON bib_view.record_num = vv3.record_num AND vv3.marc_tag = '830' AND vv3.field_content ilike '%university of northern%'
    WHERE item_status_code = '-' AND location_code = '$loc_begin' AND is_suppressed = false AND checkout.loanrule_code_num IS NULL
    AND COALESCE(call_number_norm, pe.index_entry) BETWEEN '$callnum_begin' AND '$callnum_end'
    ORDER BY CallNumber
;
"@

        # Hacky fix to query for Special Collections inventory process (different item_status_code value)
        if ($loc_begin -eq 'uspec'){
            $query = $query -Replace "'-'","'o'"
        }

        # Query the database for the range of records
        Write-Host 'Querying database for all on-shelf item records in scanned item range...'
        $item_range = Get-ODBCData -query $query -dbUser $SierraCred.Username -dbPass $SierraCred.GetNetworkCredential().Password
        
        # Use function to make the call numbers look nicer
        $item_range = Prettify-CallNumbers $item_range
        
        # Generate a comparison object. Essentially, make a new array that includes all the missing items
        $items = Compare-Object -ReferenceObject $scanned_items -DifferenceObject $item_range -Property 'Date','Time','Barcode' -includeEqual -PassThru | Sort-Object -Property ScanOrder

        # Generate an array of duplicate Call Numbers
        $dupecallnums = @($items.CallNumberFull | Group-Object | Where-Object { $_.count -gt 1 })
        
        # Initialize a bunch of variables
        $items_error_array = @()
        $count_wrongarea = 0
        $count_wrongplace = 0
        $count_missing = 0
        $last_correct_callnum = ''
        $objError = $null
        $objCorrect = [pscustomobject] @{
            Status = 'PLACEHOLDER OBJECT'
            Barcode = 'n/a'
            'Call Number' = 'A'
            'Title' = 'Placeholder object'
            'SortCallNum' = 'A'
        }
        
        # Check for errors in the scanned items list
        # Would normally use a foreach, but a regular for loop will make it easier to find mis-shelved items
        Write-Host 'Searching for errors in the scanned items list...'
        Write-Host ''
        for ($i=0; $i -lt $items.count; $i++){
            # Invalid barcode (not 14 characters)
            if ( $items[$i].barcode -NotMatch '^\d{14}$' ){
                # Add the last correct itemm to $items_error_array, if it wasn't already in
                # This check is needed as there could be multiple mis-shelved items in a row
                if ( $objCorrect.SortCallNum -ne $last_correct_callnum ){
                    $items_error_array += $objCorrect
                    $last_correct_callnum = $objCorrect.SortCallNum
                    $sort_suffix = 1
                }

                $objError = New-Object PSObject
                $objError | Add-Member -NotePropertyName 'Status' -NotePropertyValue 'Illegal barcode'
                $objError | Add-Member -NotePropertyName 'Barcode' -NotePropertyValue $items[$i].barcode.substring($items[$i].barcode.length - 7, 7)
                $objError | Add-Member -NotePropertyName 'SortCallNum' -NotePropertyValue ($objCorrect.SortCallNum + '1')
            }

            # No item record
            elseif ($items[$i].callnumberfull.length -eq 3){
                # Add the last correct item to $items_error_array, if it wasn't already in
                # This check is needed as there could be multiple mis-shelved items in a row
                if ( $objCorrect.SortCallNum -ne $last_correct_callnum ){
                    $items_error_array += $objCorrect
                    $last_correct_callnum = $objCorrect.SortCallNum
                    $sort_suffix = 1
                }
                
                $objError = New-Object PSObject
                $objError | Add-Member -NotePropertyName 'Status' -NotePropertyValue 'ERR no item rec'
                $objError | Add-Member -NotePropertyName 'Barcode' -NotePropertyValue $items[$i].barcode.substring($items[$i].barcode.length - 7, 7)
                $objError | Add-Member -NotePropertyName 'SortCallNum' -NotePropertyValue ($objCorrect.SortCallNum + $sort_suffix)
                
                $sort_suffix++
            }
            
            # Item Missing
            elseif ($items[$i].SideIndicator -eq '=>'){
                # Ignore theses when inventorying Special Collections
                if ($loc_begin -eq 'uspec' -and ($items[$i] | Get-Member -MemberType Property -Name marc_tag_830) -and $items[$i].marc_tag_830 -like '*university of northern*' -and $items[$i].marc_tag_830 -match 'dissertation|thesis|project'){
                    Write-Warning "Skipping thesis - $($items[$i].title)"
                    continue
                }

                $objError = New-Object PSObject
                $objError | Add-Member -NotePropertyName 'Status' -NotePropertyValue 'ERR missing'
                $objError | Add-Member -NotePropertyName 'Barcode' -NotePropertyValue $items[$i].barcode.substring($items[$i].barcode.length - 7, 7)
                $objError | Add-Member -NotePropertyName 'Call Number' -NotePropertyValue (Truncate-Value $items[$i].callnumberfull 25)
                $objError | Add-Member -NotePropertyName 'Title' -NotePropertyValue (Truncate-Value $items[$i].title 20)
                $objError | Add-Member -NotePropertyName 'SortCallNum' -NotePropertyValue $items[$i].callnumberfull
                
                $count_missing++
            }

            # On-shelf but Checked Out
            elseif ( $items[$i].loanrulecode.GetType().Name -eq 'Int32' ){
                # Add the last correct itemm to $items_error_array, if it wasn't already in
                # This check is needed as there could be multiple mis-shelved items in a row
                if ( $objCorrect.SortCallNum -ne $last_correct_callnum ){
                    $items_error_array += $objCorrect
                    $last_correct_callnum = $objCorrect.SortCallNum
                    $sort_suffix = 1
                }

                $objError = New-Object PSObject
                $objError | Add-Member -NotePropertyName 'Status' -NotePropertyValue 'ERR os, but co'
                $objError | Add-Member -NotePropertyName 'Barcode' -NotePropertyValue $items[$i].barcode.substring($items[$i].barcode.length - 7, 7)
                $objError | Add-Member -NotePropertyName 'Call Number' -NotePropertyValue (Truncate-Value $items[$i].callnumberfull 25)
                $objError | Add-Member -NotePropertyName 'Title' -NotePropertyValue (Truncate-Value $items[$i].title 20)
                $objError | Add-Member -NotePropertyName 'SortCallNum' -NotePropertyValue ($objCorrect.SortCallNum + $sort_suffix)
                
                $sort_suffix++
            }

            # Wrong location
            elseif ( $items[$i].location_code -ne $loc_begin ){
                # Add the last correct itemm to $items_error_array, if it wasn't already in
                # This check is needed as there could be multiple mis-shelved items in a row
                if ( $objCorrect.SortCallNum -ne $last_correct_callnum ){
                    $items_error_array += $objCorrect
                    $last_correct_callnum = $objCorrect.SortCallNum
                    $sort_suffix = 1
                }

                $status_error = 'ERR loc ' + $items[$i].location_code
                
                $objError = New-Object PSObject
                $objError | Add-Member -NotePropertyName 'Status' -NotePropertyValue $status_error
                $objError | Add-Member -NotePropertyName 'Barcode' -NotePropertyValue $items[$i].barcode.substring($items[$i].barcode.length - 7, 7)
                $objError | Add-Member -NotePropertyName 'Call Number' -NotePropertyValue (Truncate-Value $items[$i].callnumberfull 25)
                $objError | Add-Member -NotePropertyName 'Title' -NotePropertyValue (Truncate-Value $items[$i].title 20)
                $objError | Add-Member -NotePropertyName 'SortCallNum' -NotePropertyValue ($objCorrect.SortCallNum + $sort_suffix)
                
                $sort_suffix++
                $count_wrongarea++
            }

            # No call number (probably indicates an issue with SQL query)
            elseif ( $items[$i].callnumberfull.length -le 3 ){
                # Add the last correct itemm to $items_error_array, if it wasn't already in
                # This check is needed as there could be multiple mis-shelved items in a row
                if ( $objCorrect.SortCallNum -ne $last_correct_callnum ){
                    $items_error_array += $objCorrect
                    $last_correct_callnum = $objCorrect.SortCallNum
                    $sort_suffix = 1
                }

                $objError = New-Object PSObject
                $objError | Add-Member -NotePropertyName 'Status' -NotePropertyValue 'ERR no call num'
                $objError | Add-Member -NotePropertyName 'Barcode' -NotePropertyValue $items[$i].barcode.substring($items[$i].barcode.length - 7, 7)
                $objError | Add-Member -NotePropertyName 'Title' -NotePropertyValue (Truncate-Value $items[$i].title 20)
                $objError | Add-Member -NotePropertyName 'SortCallNum' -NotePropertyValue ($objCorrect.SortCallNum + '1')

                $sort_suffix++
            }
            
            # Duplicate Call Number
            elseif ( $dupecallnums.count -gt 0 -and $dupecallnums.Name -contains $items[$i].callnumberfull ){
                # Add the last correct item to $items_error_array, if it wasn't already in
                # This check is needed as there could be multiple mis-shelved items in a row
                if ( $objCorrect.SortCallNum -ne $last_correct_callnum ){
                    $items_error_array += $objCorrect
                    $last_correct_callnum = $objCorrect.SortCallNum
                    $sort_suffix = 1
                }

                $objError = New-Object PSObject
                $objError | Add-Member -NotePropertyName 'Status' -NotePropertyValue 'ERR dupe call num'
                $objError | Add-Member -NotePropertyName 'Barcode' -NotePropertyValue $items[$i].barcode.substring($items[$i].barcode.length - 7, 7)
                $objError | Add-Member -NotePropertyName 'Call Number' -NotePropertyValue (Truncate-Value $items[$i].callnumberfull 25)
                $objError | Add-Member -NotePropertyName 'Title' -NotePropertyValue (Truncate-Value $items[$i].title 20)
                $objError | Add-Member -NotePropertyName 'SortCallNum' -NotePropertyValue ($objCorrect.SortCallNum + $sort_suffix)

                $sort_suffix++
            }
            
            # Item mis-shelved
            elseif ( ( $items[$i].callnumber -lt $callnum_begin ) -or ( $items[$i].callnumber -gt $callnum_end ) -or ( $i -gt 0 -and $i -lt $items.count-1 -and ( 
                ( $items[$i].callnumber -lt $items[$i-1].callnumber -and $items[$i-1].callnumberfull.length -gt 3 -and $items[$i].callnumber -lt $items[$i+1].callnumber -and $items[$i+1].callnumberfull.length -gt 3) -or 
                ( $items[$i].callnumber -gt $items[$i-1].callnumber -and $items[$i-1].callnumberfull.length -gt 3 -and $items[$i].callnumber -gt $items[$i+1].callnumber -and $items[$i+1].callnumberfull.length -gt 3 ) ) ) ){
                # Add the last correct itemm to $items_error_array, if it wasn't already in
                # This check is needed as there could be multiple mis-shelved items in a row
                if ( $objCorrect.SortCallNum -ne $last_correct_callnum ){
                    $items_error_array += $objCorrect
                    $last_correct_callnum = $objCorrect.SortCallNum
                    $sort_suffix = 1
                }

                $objError = New-Object PSObject
                $objError | Add-Member -NotePropertyName 'Status' -NotePropertyValue 'ERR msh'
                $objError | Add-Member -NotePropertyName 'Barcode' -NotePropertyValue $items[$i].barcode.substring($items[$i].barcode.length - 7, 7)
                $objError | Add-Member -NotePropertyName 'Call Number' -NotePropertyValue (Truncate-Value $items[$i].callnumberfull 25)
                $objError | Add-Member -NotePropertyName 'Title' -NotePropertyValue (Truncate-Value $items[$i].title 20)
                $objError | Add-Member -NotePropertyName 'SortCallNum' -NotePropertyValue ($objCorrect.SortCallNum + $sort_suffix)
                
                $sort_suffix++
                
                # Item does not belong within the scanned range
                if ( ( $items[$i].callnumber -lt $callnum_begin ) -or ( $items[$i].callnumber -gt $callnum_end ) ){
                    $count_wrongarea++
                }
                # Item is mis-shelved within the scanned range
                else{
                    $count_wrongplace++
                }
                
            }
            
            # Off-shelf status, but is on shelf
            elseif ( ($loc_begin -ne 'uspec' -and $items[$i].item_status_code -ne '-' ) -or ($loc_begin -eq 'uspec' -and $items[$i].item_status_code -ne 'o') ){
                # Add the last correct itemm to $items_error_array, if it wasn't already in
                # This check is needed as there could be multiple mis-shelved items in a row
                if ( $objCorrect.SortCallNum -ne $last_correct_callnum ){
                    $items_error_array += $objCorrect
                    $last_correct_callnum = $objCorrect.SortCallNum
                    $sort_suffix = 1
                }

                $status_error = 'ERR ST = ' + $items[$i].item_status_code + ''
                
                $objError = New-Object PSObject
                $objError | Add-Member -NotePropertyName 'Status' -NotePropertyValue $status_error
                $objError | Add-Member -NotePropertyName 'Barcode' -NotePropertyValue $items[$i].barcode.substring($items[$i].barcode.length - 7, 7)
                $objError | Add-Member -NotePropertyName 'Call Number' -NotePropertyValue (Truncate-Value $items[$i].callnumberfull 25)
                $objError | Add-Member -NotePropertyName 'Title' -NotePropertyValue (Truncate-Value $items[$i].title 20)
                $objError | Add-Member -NotePropertyName 'SortCallNum' -NotePropertyValue ($objCorrect.SortCallNum + $sort_suffix)

                $sort_suffix++
            }
            
            # Item is correctly shelved. We don't need most of these in the report, but they're useful for when 'ERR msh' comes up
            else{
                $objCorrect = New-Object PSObject
                $objCorrect | Add-Member -NotePropertyName 'Status' -NotePropertyValue 'LAST ITEM SHELVED'
                $objCorrect | Add-Member -NotePropertyName 'Barcode' -NotePropertyValue $items[$i].barcode.substring($items[$i].barcode.length - 7, 7)
                $objCorrect | Add-Member -NotePropertyName 'Call Number' -NotePropertyValue (Truncate-Value $items[$i].callnumberfull 25)
                $objCorrect | Add-Member -NotePropertyName 'Title' -NotePropertyValue (Truncate-Value $items[$i].title 20)
                $objCorrect | Add-Member -NotePropertyName 'SortCallNum' -NotePropertyValue $items[$i].callnumberfull
            }
            
            # add error object to array, if one was found
            if ( $null -ne $objError ){
                $items_error_array += $objError
                $objError=$null
            }
            
        }

        #Variables used in the report
        $report_date = Get-Date -f 'ddd dd MMM yyyy'
        $report_numscanned = $scanned_items.count
        $report_numrange = $item_range.count
        $report_numoffshelf = $offshelf_items.count
        $report_dupecallnums_amt = $dupecallnums.count

        $report_output = @"
SHELF LIST INVENTORY REPORT $report_date for LOCATION $loc_begin
    
    Beginning barcode and call #   $barcode_begin      $callnumfull_begin
    Ending barcode and call #      $barcode_end      $callnumfull_end
    
    Number of items with off-shelf status              $report_numoffshelf
    Number expected to be on the shelf                 $report_numrange
    Number of items with duplicate call numbers        $report_dupecallnums_amt

    A. Number of barcodes in input file                $report_numscanned
    B. Number of barcodes in input file that
    do not belong in this area of the shelves       $count_wrongarea
    C. Number of barcodes in input file that are in
    wrong place in this area of the shelves         $count_wrongplace
    D. Number of barcodes that are MISSING from
    the input file                                  $count_missing

ERROR REPORT FOR LOCATION Stacks

"@
        
        # Adjust the SortCallNum value in $items_error_array for correct sorting
        # The first number in an LC call number must be treated as a whole number, possible values from 1 to 9999
        # Three replaces are performed, to insert different amounts of leading zeros, as needed
        foreach ($item in $items_error_array){
            $item.SortCallNum = $item.SortCallNum -Replace "^(?<l>[A-Z]+)(?<n>\d{1}\s)", '${l}000${n}' -Replace "^(?<l>[A-Z]+)(?<n>\d{2}\s)", '${l}00${n}' -Replace "^(?<l>[A-Z]+)(?<n>\d{3}\s)", '${l}0${n}'
        }
        
        # Write Report Output for current input file
        Write-Host 'Writing Report file...'
        Write-Host ''
        $ReportFile = $file.BaseName + '_InventoryReport.txt'
        $report_output | Out-File $ReportFile -Encoding ASCII
        $items_error_array | Sort-Object -property SortCallNum | Select-Object Status,Barcode,'Call Number',Title | Out-File $ReportFile -Encoding ASCII -Append
        
        # Append Items List to file, to be used for import into Sierra Review File
        Write-Host 'Adding to ItemList file...'
        Write-Host ''
        $items | ForEach-Object item_record | Out-File $ReviewFile -Encoding ASCII -Append
        
        # Display report summary and pause for user interaction
        Write-Host $report_output
        Write-Host 'Press a key to continue...' -ForegroundColor Yellow
        $pause = $host.ui.RawUI.ReadKey('NoEcho,IncludeKeyDown')
    }
}
catch{
    Write-Warning 'Error encountered. Please notify ITS.'
    Write-Host $_
    Write-Host '' 

    # Pause for interaction before closing window
    Write-Host 'Press a key to continue...' -ForegroundColor Yellow
    $pause = $host.ui.RawUI.ReadKey('NoEcho,IncludeKeyDown')
}