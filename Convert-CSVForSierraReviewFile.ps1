<#
Convert-CSVForSierraReviewFile script
Written by David Pettitt

Written for Vaunda Dumont (Circulation Supervisor)

Converts files created by CS3070 "phaser" scanner or Cognex Scanner barcode scanner iOS app into a format that can be Imported into a Sierra Review File. Basically, it takes a list of barcodes and queries the Sierra database to get a list of item numbers.

For this script to work, you need to download and install an x64 ODBC driver for PostgreSQL from https://www.postgresql.org/ftp/odbc/versions/msi/
No configuration in Windows is necessary after installing the driver

Reference:
http://blog.briankmarsh.com/postgresql-powershell-part-1/

#>

# This is for PostgreSQL queries that return stuff
function Get-ODBCData{  
    param(
          [string]$query,    # Query
          [string]$dbServer = 'wizarddb.unbc.ca',   # DB Server (either IP or hostname)
          [int]$dbPort = 1032, # DB Connection Port
          [string]$dbName   = 'iii', # Name of the database
          [string]$dbUser   = 'reports',    # User we'll use to connect to the database/server
          [string]$dbPass    # Password for the $dbUser
         )

    $conn = New-Object System.Data.Odbc.OdbcConnection
    $conn.ConnectionString = "Driver={PostgreSQL Unicode(x64)};Server=$dbServer;Port=$dbPort;Database=$dbName;Uid=$dbUser;Pwd=$dbPass;sslmode=require;"
    $conn.open()
    $cmd = New-object System.Data.Odbc.OdbcCommand($query,$conn)
    $ds = New-Object system.Data.DataSet
    (New-Object system.Data.odbc.odbcDataAdapter($cmd)).fill($ds) | out-null
    $conn.close()
    $ds.Tables[0]
}

# Output filename
$csv_output_file = 'SierraItemList.csv'

# Get the item(s) to process
if (Test-Path ReviewFileTest){
	$CSVFiles = Get-ChildItem ReviewFileTest\*.csv
}
else{
	$CSVFiles = Get-ChildItem *.csv
}

# Get the Sierra credentials that will be used to connect to database
# Load from file if possible. If file doesn't exist, prompt and then save to file for next time
$CredFile = 'Sierra_DB_Credentials_' + $env:Username + '_' + $env:ComputerName + '.cred'
if (Test-Path $CredFile){
	$SierraCred = Import-CliXML -Path $CredFile
}
else{
	$SierraCred = Get-Credential -message 'Enter Sierra login credentials:'
	$SierraCred | Export-CliXML -Path $CredFile
}

# Variable to hold all joined barcodes
$barcodes_joined = ''

# Variable to hold count of input barcodes
$barcodes_count = 0

# Get the barcodes from all the .csv files
foreach ($file in $CSVFiles){
    # Import contents of .csv file
	$csv = Import-CSV $file
	$LineCount = (Get-Content $File | Measure-Object -Line).Lines
	
	# Check if it's a Mobile Worklists file, with proper headers
	if ($LineCount -gt 1 -and $csv.count -gt 0 -and $null -ne $csv[0].barcode){
		Write-Host ''
		Write-Host 'No need to convert '$file', it is already ready for import.'
		Write-Host ''
	}
	# Check if it's from Cognex Scanner iOS app
	elseif ($LineCount -gt 1 -and $csv.count -gt 0 -and $null -ne $csv[0].'BarcodeScanners History'){
		$csv = @()
		$csv += Get-Content $file | Select-Object -Skip 2 | ConvertFrom-CSV
		# Remove extraneous final entry
		$csv = $csv | Where-Object {$_.Symbology -ne 'Cognex Corporation'}
		# Merge barcodes into single string, to use in SQL query
		$barcodes_joined += ($csv.Code -join "'),('")
		$barcodes_count += $csv.Code.count
	}
	# Assumes it's a CS3070 "phaser" scanner file
	else{
		$csv = @()
		$csv += Import-CSV $file -Header 'Date','Time','Blah','Barcode'
		
		# Confirm we actually have some valid barcodes to work with
		if ($LineCount -gt 0 -and $csv.count -gt 0 -and $csv.barcode -match '\d{14}') {
			# Merge barcodes into single string, to use in SQL query
			$barcodes_joined += ($csv.barcode -join "'),('") 
			$barcodes_count += @($csv.barcode).count
		}
		else{
			Write-Host ''
			Write-Host 'No valid barcodes found in' $file.Name
			Write-Host ''
		}
	}
}

if ($barcodes_joined.length -gt 0){
	$barcodes_query = "('" + $barcodes_joined + "')"
	# Clean up barcodes list to remove empty entries
	$barcodes_query = $barcodes_query -Replace "\(''\)",'' -Replace ',,',','

	# SQL query to get records for all scanned barcodes
	$query = @"
SELECT 'i' || item_view.record_num AS item_record,
 item_view.barcode,
 item_view.location_code,
 bib_view.title
	FROM (
		values $barcodes_query
	) scanned(scanned_barcode)
	LEFT JOIN sierra_view.item_view AS item_view ON item_view.barcode = scanned.scanned_barcode
	LEFT JOIN sierra_view.bib_record_item_record_link AS brirl ON item_view.id = brirl.item_record_id
	LEFT JOIN sierra_view.bib_view AS bib_view ON brirl.bib_record_id = bib_view.id
;
"@

	# Query the database for the provided list of scanned barcodes
	Write-Host 'Querying database for scanned item details...'
	Write-Host ''
	$scanned_items = Get-ODBCData -query $query -dbUser $SierraCred.Username -dbPass $SierraCred.GetNetworkCredential().Password

	# Export db query results to .csv
	$scanned_items | Export-CSV $csv_output_file -NoTypeInformation
	
	Get-Date
	Write-Host 'Output file:' $csv_output_file
	Write-Host '# of input records:' $barcodes_count
	Write-Host '# of output records:' $scanned_items.item_record.count
	Write-Host ''
}
else {
	Get-Date
	Write-Host 'No records to convert'
	Write-Host ''
}

Write-Host 'Press a key to continue...' -ForegroundColor Yellow
$pause = $host.ui.RawUI.ReadKey('NoEcho,IncludeKeyDown')